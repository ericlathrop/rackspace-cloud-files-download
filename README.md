# rackspace-cloud-files-download

Download all files from a Rackspace Cloud Files Container.

Migrating away from Rackspace? These scripts will help you download all files in
a Cloud Files Container because I couldn't find a way to do this.

# Usage

1. Edit `config.json` to specify your container name.
2. Edit `rackspace-config.json` to specify your Rackspace username and API key.
3. Run `node list-files.js config.json`. This will download a list of all files
   in the container and save it locally to `files.json`. This is a big speedup
   when you have many files and need to restart the downloader multiple times
   because of errors.
4. Run `node download-files.js config.json`. This will download all the files in
   `files.json`, skipping files that already exist.
