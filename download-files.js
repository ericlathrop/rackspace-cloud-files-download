const async = require("async");
const fsPromises = require("fs").promises;
const pkgcloud = require("pkgcloud");
const util = require("util");

const rackspaceConfig = require("./rackspace-config");
const client = pkgcloud.storage.createClient(rackspaceConfig);
const download = util.promisify(client.download).bind(client);

const args = process.argv.slice(2);
if (args.length !== 1) {
  console.error("Usage: node download-files.js config.json");
  process.exit(1);
}
const configPath = args[0];

readJsonFile(configPath)
  .then(downloadAllFiles);

function readJsonFile(path) {
  return fsPromises.readFile(path, { encoding: "utf8" })
    .then(data => JSON.parse(data));
}

function downloadAllFiles(config) {
  return readJsonFile(config.fileListPath)
    .then(files => async.eachOfLimit(files, config.simultaneousDownloads, (file, index, callback) => downloadOneFile(config, files.length, file, index, callback)))
}

function downloadOneFile(config, totalFiles, file, index, callback) {
  const containerName = config.container;
  const localName = `${config.downloadPath}/${file}`;
  const progress = `File ${index} of ${totalFiles} (${Math.round(index / totalFiles * 100)}%)`;

  return fsPromises.stat(localName).then(stats => {
    console.log(`${progress} - Already downloaded ${file}`);
    callback(undefined, stats);
  }).catch(err => {
    console.log(`${progress} - Downloading ${file}`);

    const options = {
      container: containerName,
      remote: file,
      local: localName
    };
    return download(options)
      .then(() => callback())
      .catch(err => {
        console.log(`${progress} - Error downloading ${file}: ${err}`);
        callback();
      });
  });
}
