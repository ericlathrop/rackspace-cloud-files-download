const fsPromises = require("fs").promises;
const pkgcloud = require("pkgcloud");
const util = require("util");

const rackspaceConfig = require("./rackspace-config");
const client = pkgcloud.storage.createClient(rackspaceConfig);
const getContainer = util.promisify(client.getContainer).bind(client);
const getFiles = util.promisify(client.getFiles).bind(client);

const args = process.argv.slice(2);
if (args.length !== 1) {
  console.error("Usage: node list-files.js config.json");
  process.exit(1);
}

const configPath = args[0];

readJsonFile(configPath)
  .then(getFileList);

function readJsonFile(path) {
  return fsPromises.readFile(path, { encoding: "utf8" })
    .then(data => JSON.parse(data));
}

function getFileList(config) {
  const containerName = config.container;

  return getContainer(containerName)
    .then(container => getFiles(container, { limit: Infinity }))
    .then(files => files.map(({ name }) => name))
    .then(files => fsPromises.writeFile(config.fileListPath, JSON.stringify(files)));
}
